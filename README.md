# rust_game
Trying my luck at game development with` Rust` using the `Amethyst` engine.  
This repository contains some example code, made as clean as possible.  
Rust edition: `2018`  

Amethyst: https://github.com/amethyst/amethyst