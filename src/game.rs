use crate::player::*;

use amethyst::assets::*;
use amethyst::ui::*;
use amethyst::ecs::*;
use amethyst::core::*;
use amethyst::prelude::*;
use amethyst::renderer::*;

pub const ARENA_WIDTH: f32 = 320.0;
pub const ARENA_HEIGHT: f32 = 240.0;

pub struct Game;

impl SimpleState for Game {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;
        let sprite_sheet_handle = load_sprite_sheet(world);
        world.delete_all();

        init_player(world, sprite_sheet_handle);
        init_camera(world);
    }

    fn handle_event(&mut self, _data: StateData<'_, GameData<'_, '_>>, event: StateEvent) -> Trans<GameData<'static, 'static>, StateEvent> {
        if let StateEvent::Window(e) = &event {
            if amethyst::input::is_key_down(e, VirtualKeyCode::Return) {
                _data.world.delete_all();
                return Trans::Pop
            }
        }

        Trans::None
    }
}

fn init_camera(world: &mut World) {
    let mut transform = transform::Transform::default();
    transform.set_z(1.0);

    world
        .create_entity()
        .with(Camera::from(Projection::orthographic(
            0.0,
            ARENA_WIDTH,
            0.0,
            ARENA_HEIGHT,
        )))
        .with(transform)
        .build();
}

fn init_text(world: &mut World) {
    let font = world.read_resource::<Loader>().load(
        format!("{}/resources/font/square.ttf", amethyst::utils::application_root_dir()),
        TtfFormat,
        Default::default(),
        (),
        &world.read_resource(),
    );

    let transform = UiTransform::new(
        "TEXT".to_string(), Anchor::TopMiddle,
        0.0, -100.0, 1.0, 500.0, 50.0, 1
    );

    world.create_entity()
        .with(transform)
        .with(UiText::new(
            font.clone(),
            "Press ENTER to begin".to_string(),
            [1., 1., 1., 1.],
            50.,
        )).build();

}

pub struct Loading;

impl SimpleState for Loading {
    fn on_start(&mut self, _data: StateData<'_, GameData<'_, '_>>) {
        let world = _data.world;

        init_text(world);
    }

    fn on_resume(&mut self, _data: StateData<'_, GameData<'_, '_>>) {
        init_text(_data.world);
    }

    fn handle_event(&mut self, _data: StateData<'_, GameData<'_, '_>>, event: StateEvent) -> Trans<GameData<'static, 'static>, StateEvent> {
        if let StateEvent::Window(e) = &event {
            if amethyst::input::is_key_down(e, VirtualKeyCode::Return) {
                return Trans::Push(Box::new(Game));
            }
        }

        Trans::None
    }


}