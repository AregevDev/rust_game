#![windows_subsystem = "windows"]

mod game;
mod player;
mod systems;

use crate::game::*;

use amethyst::*;
use amethyst::ui::*;
use amethyst::core::*;
use amethyst::input::*;
use amethyst::utils::*;
use amethyst::prelude::*;
use amethyst::renderer::*;

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let display_path = format!("{}/resources/display_config.ron", application_root_dir());
    let conf = DisplayConfig::load(&display_path);

    let binding_path = format!(
        "{}/resources/bindings_config.ron",
        application_root_dir()
    );

    let input_bundle = InputBundle::<String, String>::new()
        .with_bindings_from_file(binding_path)?;

    let pipe = Pipeline::build()
        .with_stage(
            Stage::with_backbuffer()
                .clear_target([0.05, 0.05, 0.05, 1.0], 1.0)
                .with_pass(DrawFlat2D::new())
                .with_pass(DrawUi::new()),
    );

    let data = GameDataBuilder::default()
        .with_bundle(
            RenderBundle::new(pipe, Some(conf))
                .with_sprite_sheet_processor()
        )?
        .with_bundle(transform::TransformBundle::new())?
        .with_bundle(UiBundle::<String, String>::new())?
        .with_bundle(input_bundle)?
        .with(systems::PlayerSystem, "player_system", &["input_system"]);

    let mut game = Application::new("./", Loading, data)?;
    game.run();

    Ok(())
}