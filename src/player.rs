use crate::game;

use amethyst::ecs::*;
use amethyst::core::*;
use amethyst::utils::*;
use amethyst::assets::*;
use amethyst::renderer::*;

pub struct Player {
    pub width: f32,
    pub height: f32,
}

impl Player {
    pub fn new(width: f32, height: f32) -> Self {
        Player { width, height }
    }
}

impl Component for Player {
    type Storage = DenseVecStorage<Self>;
}

pub fn init_player(world: &mut World, sprite_sheet: SpriteSheetHandle) {
    let player = Player::new(32.0, 32.0);
    let mut transform = transform::Transform::default();
    transform.set_xyz(game::ARENA_WIDTH * 0.5, game::ARENA_HEIGHT * 0.5, 0.0);

    let sprite_render = SpriteRender {
        sprite_sheet: sprite_sheet.clone(),
        sprite_number: 0, // paddle is the first sprite in the sprite_sheet
    };

    world
        .create_entity()
        .with(sprite_render.clone())
        .with(player)
        .with(transform)
        .build();
}

pub fn load_sprite_sheet(world: &mut World) -> SpriteSheetHandle {

    let tex_handle = {
        let tex_loader = world.read_resource::<Loader>();
        let tex_storage = world.read_resource::<AssetStorage<Texture>>();

        tex_loader.load(
            format!("{}/resources/texture/game_spritesheet.png", application_root_dir()),
            PngFormat,
            TextureMetadata::srgb_scale(),
            (),
            &tex_storage,
        )
    };

    let loader = world.read_resource::<Loader>();
    let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
    loader.load(
        format!("{}/resources/texture/game_spritesheet.ron", application_root_dir()),
        SpriteSheetFormat,
        tex_handle,
        (),
        &sprite_sheet_store,
    )
}