use crate::game::*;
use crate::player::*;

use amethyst::ecs::*;
use amethyst::core::*;
use amethyst::input::*;

pub struct PlayerSystem;

impl<'a> System<'a> for PlayerSystem {
    type SystemData = (
        WriteStorage<'a, Transform>,
        ReadStorage<'a, Player>,
        Read<'a, InputHandler<String, String>>,
    );

    fn run(&mut self, (mut transforms, players, input): Self::SystemData) {
        for (player, transform) in (&players, &mut transforms).join() {
            let move_up = input.axis_value("player_up");
            let move_right = input.axis_value("player_right");

            if let Some(v) = move_up {
                let scaled_amount = v as f32;
                let paddle_y = transform.translation().y;
                transform.set_y(
                    (paddle_y + scaled_amount)
                        .min(ARENA_HEIGHT - player.height * 0.5)
                        .max(player.height * 0.5),
                );
            }

            if let Some(v) = move_right {
                let scaled_amount = v as f32;
                let paddle_x = transform.translation().x;
                transform.set_x(
                    (paddle_x + scaled_amount)
                        .min(ARENA_WIDTH - player.width * 0.5)
                        .max(player.width * 0.5),
                );
            }
        }
    }
}